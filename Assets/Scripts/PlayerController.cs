﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    Animator animator;
    Rigidbody2D rigid2D;
    public float jumpForce = 400.0f;
    
    public bool onDamage = false;
    private SpriteRenderer renderer;
    public float moveSpeed = 1f;

    public int life = 10;

    


    // Start is called before the first frame update
    void Start()
    {

        this.rigid2D = GetComponent<Rigidbody2D>();
        this.animator = GetComponent<Animator>();

        renderer = gameObject.GetComponent<SpriteRenderer>();


    }

    // Update is called once per frame
    void Update()
    {
        //ジャンプ
        if (Input.GetKeyDown(KeyCode.Space))
        {
            this.animator.SetTrigger("JumpTrigger");
            this.rigid2D.AddForce(transform.up * jumpForce);
        }
        int key = 0;

        //横移動
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            key = -1;
            transform.Translate(-1 * moveSpeed, 0, 0);
            this.animator.speed = 1.0f;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            key = 1;
            transform.Translate(moveSpeed, 0, 0);
            this.animator.speed = 1.0f;
            
        }

        //反転防止
        if (key != 0)
        {
            transform.localScale = new Vector3(key, 1, 1);
        }

       
        //被弾処理
        if (onDamage)
        {
            float level = Mathf.Abs(Mathf.Sin(Time.time * 10));
            renderer.color = new Color(1f, 1f, 1f, level);
        }

        //ゲームオーバー

        if (life <= 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
        if(transform.position.y < -11f)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }

    
    private void OnTriggerEnter2D(Collider2D other)
    {
        //接敵
        if(other.gameObject.tag == "enemy")
        {
            life -= 1;
            GameObject director = GameObject.Find("GameDirector");
            director.GetComponent<GameDirector>().DecreaseHp();

            onDamage = true;

            StartCoroutine("WaitForIt");

        }
        //ゴール
        if(other.gameObject.tag =="flag")
        {
            SceneManager.LoadScene("ClearScene");
        }
    }
    //点滅処理
    IEnumerator WaitForIt()
    {
        yield return new WaitForSeconds(1);
        onDamage = false;
        renderer.color = new Color(1f, 1f, 1f, 1f);
    }
}