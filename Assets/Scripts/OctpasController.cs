﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OctpasController : MonoBehaviour
{
    public float moveRopeZone = 2.0f;
    public float moveSpeed = 2.0f;
    bool m_yPlus = true;
    Vector2 vecBasePos;
    // Start is called before the first frame update
    void Start()
    {
        vecBasePos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_yPlus)
        {
            transform.position += new Vector3(0f, moveSpeed * 1.0f * Time.deltaTime, 0f);
            if (transform.position.y - vecBasePos.y >= moveRopeZone)
                m_yPlus = false;
            
        }
        else
        {
            transform.position -= new Vector3( 0f, moveSpeed * 1.0f * Time.deltaTime, 0f);
            if (transform.position.y - vecBasePos.y <= moveRopeZone * -1)
                m_yPlus = true;
        }
    }
}
