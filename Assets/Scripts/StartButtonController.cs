﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartButtonController : MonoBehaviour
{
    public void ChangeCcene(string scene)
    {
        switch (scene)
        {
            case "Title":
                SceneManager.LoadScene("Title");
                break;
            case "GameScene":
                SceneManager.LoadScene("GameScene");
                break;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
