﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
    GameObject hpUI;
    
    void Start()
    {
        this.hpUI = GameObject.Find("hpUI");
    }

    public void DecreaseHp()
    {
        this.hpUI.GetComponent<Image>().fillAmount -= 0.1f;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}