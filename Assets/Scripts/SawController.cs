﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawController : MonoBehaviour
{
    public float moveRopeZone = 2.0f;
    public float moveSpeed = 2.0f;
    bool m_xPlus = true;
    Vector2 vecBasePos;
    // Start is called before the first frame update
    void Start()
    {
        vecBasePos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, 2);

        if (m_xPlus)
        {
            transform.position += new Vector3(moveSpeed * 1.0f * Time.deltaTime, 0f, 0f);
            if (transform.position.x - vecBasePos.x >= moveRopeZone)
                m_xPlus = false;
            transform.localScale = new Vector3(-4, 4, 1);
        }
        else
        {
            transform.position -= new Vector3(moveSpeed * 1.0f * Time.deltaTime, 0f, 0f);
            if (transform.position.x - vecBasePos.x <= moveRopeZone * -1)
                m_xPlus = true;

            transform.localScale = new Vector3(4, 4, 1);
        }
    }
}
